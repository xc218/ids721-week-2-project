## Lambda Function Explanation
This lambda function can examine the 'operation' from input JSON to determine add or substract, and operate on 'number1' and 'number2'.

## Deploy Lambda Function
I deployed this lambda function on AWS and test it.
When the input is:
![test_json](image/test_json.png)

The output is:
![test_result](image/test_result.png)

## API Gateway Integration
This lambda function is connected with an API gateway as shows:
![api_connect](image/api_connect.png)

I built a REST API and test its functionality.
The input header is 'application/json', and the input is:
![api_input](image/api_input.png)

And the output is:
![api_test](image/api_test.png) 

It is clear that the result is 42 which is the sum of 15.0 and 27.0.

The invoke URL is https://uab4u7nw1j.execute-api.us-east-2.amazonaws.com/dev