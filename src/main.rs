use lambda_runtime::{service_fn, LambdaEvent, Error};
use serde_json::{json, Value};

async fn function_handler(event: LambdaEvent<Value>) -> Result<Value, Error> {
    // Extract the event and context from LambdaEvent
    let (event, _context) = event.into_parts();
    let operation = event["operation"].as_str().unwrap_or("add");
    let number1 = event["number1"].as_f64().unwrap_or(0.0);
    let number2 = event["number2"].as_f64().unwrap_or(0.0);

    let result = match operation {
        "subtract" => number1 - number2,
        _ => number1 + number2, 
    };

    Ok(json!({ "result": result }))
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    // Use `service_fn` instead of the deprecated `handler_fn`
    lambda_runtime::run(service_fn(function_handler)).await?;
    Ok(())
}
